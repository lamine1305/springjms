package dz.messagerie.messagerie.controler;

import dz.messagerie.messagerie.entite.User;
import dz.messagerie.messagerie.pojo.UserPojo;
import dz.messagerie.messagerie.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class MessageControler {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public void addUser(@RequestBody UserPojo userPojo){
        userService.postMessage(userPojo);
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public List<User> getUsers(){
      return  userService.getAllUsers();
    }

}
