package dz.messagerie.messagerie.service;

import dz.messagerie.messagerie.entite.User;
import dz.messagerie.messagerie.pojo.UserPojo;
import dz.messagerie.messagerie.prodCons.ProducerResource;
import dz.messagerie.messagerie.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    ProducerResource producerResource ;

    public List<User> getAllUsers(){
       return userRepository.findAll();
    }

    public void postMessage(UserPojo userPojo) {
        producerResource.postMessage(userPojo);
    }
}
