package dz.messagerie.messagerie.prodCons;

import com.fasterxml.jackson.databind.ObjectMapper;
import dz.messagerie.messagerie.pojo.UserPojo;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class ProducerResource {
    @Autowired
    JmsTemplate jmsTemplate;

    @Autowired
    ActiveMQQueue queue;

    public void postMessage(UserPojo userPojo ) {
        try {
            System.out.println(" -> ->   "+userPojo.getLogin());
            ObjectMapper mapper = new ObjectMapper();
            String userJson = mapper.writeValueAsString(userPojo);
            System.out.println(" -> ->   "+userJson);
            jmsTemplate.convertAndSend(queue, userJson);
            System.out.println("messages envoyés\n");
        } catch (Exception e) {
            e.printStackTrace();
        }

        }

}
