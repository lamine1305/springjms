package dz.messagerie.messagerie.prodCons;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dz.messagerie.messagerie.entite.User;
import dz.messagerie.messagerie.pojo.UserPojo;
import dz.messagerie.messagerie.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;


@Component
public class ConsumerResource{

    @Autowired
    UserRepository userRepository;

    @JmsListener(destination = "Lamine.queue")
    public void consume(String m) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        UserPojo userPojo = objectMapper.readValue(m,UserPojo.class);
        convertUserPojoToEntity(userPojo);
    }

    private void convertUserPojoToEntity(UserPojo userPojo) {
        User user = new User();
        user.setId(userPojo.getId());
        user.setLogin(userPojo.getLogin());
        user.setPassword(userPojo.getPassword());

        userRepository.save(user);
    }


}
